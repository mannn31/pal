<?php
require_once("laporan/fpdf/fpdf.php");
require_once("koneksi.php");

class PDF extends FPDF
{
    // Page header
    function Header()
    {
      // Logo
      $this->Image('gambar/logosumedang.jpeg',20,10);

    	// Arial bold 15
    	$this->SetFont('Times','B',15);
    	// Move to the right
    	// $this->Cell(60);
    	// Title
        $this->Cell(308,8,'PEMERINTAH KABUPATEN SUMEDANG',0,1,'C');
        $this->Cell(308,8,'KECAMATAN JATINANGOR',0,1,'C');
    	$this->Cell(308,8,'',0,1,'C');
    	// Line break
    	$this->Ln(5);

        $this->SetFont('Times','BU',12);
        for ($i=0; $i < 10; $i++) {
            $this->Cell(308,0,'',1,1,'C');
        }

        $this->Ln(1);

        $this->Cell(308,8,'LAPORAN DATA PENDUDUK TETAP',0,1,'C');
        $this->Ln(2);

        $this->SetFont('Times','B',9.5);

        // header tabel
        $this->cell(8,7,'NO.',1,0,'C');
        $this->cell(30,7,'NIK',1,0,'C');
        $this->cell(40,7,'NAMA',1,0,'C');
        $this->cell(30,7,'TEMPAT LAHIR',1,0,'C');
        $this->cell(24,7,'TGL. LAHIR',1,0,'C');
        $this->cell(8,7,'JK',1,0,'C');
        $this->cell(50,7,'ALAMAT',1,0,'C');
        $this->cell(30,7,'KECAMATAN',1,0,'C');
        $this->cell(30,7,'KABUPATEN',1,0,'C');
        $this->cell(30,7,'PROVINSI',1,0,'C');
        $this->cell(26,7,'PERNIKAHAN',1,1,'C');
       // $this->cell(27,7,'PDDKN',1,0,'C');
       // $this->cell(25,7,'KERJA',1,0,'C');
       // $this->cell(27,7,'STATUS',1,1,'C');

    }

    // Page footer
    function Footer()
    {
    	// Position at 1.5 cm from bottom
    	$this->SetY(-15);
    	// Arial italic 8
    	$this->SetFont('Arial','I',8);
    	// Page number
    	$this->Cell(0,10,'Page '.$this->PageNo().'/{nb}',0,0,'C');
    }
}

// ambil dari database
$query = "SELECT * FROM v_tetap";
$hasil = mysqli_query($konek, $query);
$data_warga = array();
while ($row = mysqli_fetch_assoc($hasil)) {
  $data_warga[] = $row;
}


$pdf = new PDF('L', 'mm', [210, 330]);
$pdf->AliasNbPages();
$pdf->AddPage();

// set font
$pdf->SetFont('Times','',9);

// set penomoran
$nomor = 1;

foreach ($data_warga as $warga) {
    $pdf->cell(8, 7, $nomor++ . '.', 1, 0, 'C');
    $pdf->cell(30, 7, strtoupper($warga['nik']), 1, 0, 'C');
    $pdf->cell(40, 7, substr(strtoupper($warga['nama']),0 ,20), 1, 0, 'L');
    $pdf->cell(30, 7, strtoupper($warga['tempat_lahir']), 1, 0, 'C');
    $pdf->cell(24, 7, ($warga['tgl_lahir'] != '0000-00-00') ? date('d-m-Y', strtotime($warga['tgl_lahir'])) : '', 1, 0, 'C');
    $pdf->cell(8, 7, substr(strtoupper($warga['jenis_kelamin']), 0, 1), 1, 0, 'C');
    //$pdf->cell(8, 7, strtoupper($warga['usia_warga']), 1, 0, 'C');
    $pdf->cell(50, 7, substr(strtoupper($warga['alamat']), 0, 20), 1, 0, 'L');
    $pdf->cell(30, 7, strtoupper($warga['kecamatan']), 1, 0, 'C');
    $pdf->cell(30, 7, strtoupper($warga['kabupaten']), 1, 0, 'C');
    $pdf->cell(30, 7, strtoupper($warga['provinsi']), 1, 0, 'C');
    $pdf->cell(26, 7, strtoupper($warga['status_kawin']), 1, 1, 'C');
   // $pdf->cell(27, 7, strtoupper($warga['pendidikan_terakhir_warga']), 1, 0, 'C');
    //$pdf->cell(25, 7, strtoupper($warga['pekerjaan_warga']), 1, 0, 'C');
    //$pdf->cell(27, 7, strtoupper($warga['status_warga']), 1, 1, 'C');
}

	$pdf->Ln(10);

$pdf->Output();
?>
