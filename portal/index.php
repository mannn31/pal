<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8" />
  <link rel="apple-touch-icon" sizes="76x76" href="assets/img/apple-icon.png">
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
  <link rel="icon" type="image/png" href="assets/img/Logo SMK.png">
  <title>
    SMK PGRI 35 SOLOKANJERUK
  </title>
  <meta name="viewport" content="initial-scale=1">
  <!--     Fonts and icons     -->
  <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Roboto+Slab:400,700|Material+Icons" />
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css">
  <!-- CSS Files -->
  <link href="assets/css/material-dashboard.min.css" rel="stylesheet" />
  <!-- CSS Just for demo purpose, don't include it in your project -->
  
 
  <div class="content-wrapper" style="background:url('assets/img/Backgroundsmkpgri.png');background-size: cover;">
  
    <div class="container">
      <!-- Content Header (Page header) -->
      <br>
     <section class="content-header">

          <center> <img src="assets/img/smkpgri.png" alt="" style="width:120px; height:120px"></center>
           <font color='black'><center><b>SMK PGRI 35 SOLOKANJERUK</b></center></font>
     

    <div class="container-fluid">
      <div class="row">
        <div class="col-md-4">
          <div class="card card-product">
            <div class="card-header card-header-image" data-header-animation="true">
              <a href="#">
                <img class="img" src="assets/img/iconweb.png">
              </a>
            </div>
            <div class="card-body">
              <div class="card-actions text-center">                  
                <button type="button" class="btn btn-default btn-link" rel="tooltip" data-placement="bottom" title="Login">
                  <a href="https://smkpgri35soljer.career.support/">

            <div class="card-header-primary card-header-icon">
                    <div class="card-icon">
                <i class="material-icons"></i>Kunjungi        
                 
                </button>
                
              </div>
              <h4 class="card-title">
                <a><b>Website SMK PGRI 35 SOLOKANJERUK</b></a>
              </h4>
              <div class="card-description">
                Silahkan Kunjungi ke Website SMK PGRI 35 SOLOKANJERUK
              </div>
            </div>
            <div >
              <div >
                <!-- <h4>$899/night</h4> -->
              </div>
              <div >
                
              </div>
            </div>
          </div>
        </div>
        <div class="col-md-4">
          <div class="card card-product">
            <div class="card-header card-header-image" data-header-animation="true">
              <a href="#">
                <img class="img" src="assets/img/iconppdb.png">
              </a>
            </div>
            <div class="card-body">
              <div class="card-actions text-center">                  
                <button type="button" class="btn btn-default btn-link" rel="tooltip" data-placement="bottom" title="Login">
                  <a href="https://smkpgri35soljer.career.support/">

            <div class="card-header-primary card-header-icon">
                    <div class="card-icon">
                <i class="material-icons"></i>Kunjungi        
                 
                </button>
                
              </div>
              <h4 class="card-title">
                <a><b>PPDB SMK PGRI 35 SOLOKANJERUK</b></a>
              </h4>
              <div class="card-description">
                Silahkan Kunjungi ke pendaftaran peserta didik baru SMK PGRI 35 SOLOKANJERUK
              </div>
            </div>
            <div >
              <div >
                <!-- <h4>$899/night</h4> -->
              </div>
              <div >
                
              </div>
            </div>
          </div>
        </div>
        <div class="col-md-4">
          <div class="card card-product">
            <div class="card-header card-header-image" data-header-animation="true">
              <a href="#">
                <img class="img" src="assets/img/iconelearn.png">
              </a>
            </div>
            <div class="card-body">
              <div class="card-actions text-center">                  
                <button type="button" class="btn btn-default btn-link" rel="tooltip" data-placement="bottom" title="Login">
                  <a href="https://smkpgri35soljer.career.support/">

            <div class="card-header-primary card-header-icon">
                    <div class="card-icon">
                <i class="material-icons"></i>Kunjungi        
                 
                </button>
                
              </div>
              <h4 class="card-title">
                <a><b>Elearning SMK PGRI 35 SOLOKANJERUK</b></a>
              </h4>
              <div class="card-description">
                Silahkan Kunjungi ke Elearning SMK PGRI 35 SOLOKANJERUK
              </div>
            </div>
            <div >
              <div >
                <!-- <h4>$899/night</h4> -->
              </div>
              <div >
                
              </div>
            </div>
          </div>
        </div>
        <div class="col-md-4">
          <div class="card card-product">
            <div class="card-header card-header-image" data-header-animation="true">
              <a href="#">
                <img class="img" src="assets/img/iconsiakad.png">
              </a>
            </div>
            <div class="card-body">
              <div class="card-actions text-center">                  
                <button type="button" class="btn btn-default btn-link" rel="tooltip" data-placement="bottom" title="Login">
                  <a href="https://smkpgri35soljer.career.support/">

            <div class="card-header-primary card-header-icon">
                    <div class="card-icon">
                <i class="material-icons"></i>Kunjungi        
                 
                </button>
                
              </div>
              <h4 class="card-title">
                <a><b>Siakad SMK PGRI 35 SOLOKANJERUK</b></a>
              </h4>
              <div class="card-description">
                Silahkan Kunjungi ke Sistem Informasi Akademik SMK PGRI 35 SOLOKANJERUK
              </div>
            </div>
            <div >
              <div >
                <!-- <h4>$899/night</h4> -->
              </div>
              <div >
                
              </div>
            </div>
          </div>
        </div>
        <div class="col-md-4">
          <div class="card card-product">
            <div class="card-header card-header-image" data-header-animation="true">
              <a href="#">
                <img class="img" src="assets/img/iconkeuangan.png">
              </a>
            </div>
            <div class="card-body">
              <div class="card-actions text-center">                  
                <button type="button" class="btn btn-default btn-link" rel="tooltip" data-placement="bottom" title="Login">
                  <a href="https://smkpgri35soljer.career.support">

            <div class="card-header-primary card-header-icon">
                    <div class="card-icon">
                <i class="material-icons"></i>Kunjungi        
                 
                </button>
                
              </div>
              <h4 class="card-title">
                <a><b>Keuangan SMK PGRI 35 SOLOKANJERUK</b></a>
              </h4>
              <div class="card-description">
                Silahkan Kunjungi ke Website Keuangan SMK PGRI 35 SOLOKANJERUK
              </div>
            </div>
            <div >
              <div >
                <!-- <h4>$899/night</h4> -->
              </div>
              <div >
                
              </div>
            </div>
          </div>
        </div>
        <div class="col-md-4">
          <div class="card card-product">
            <div class="card-header card-header-image" data-header-animation="true">
              <a href="#">
                <img class="img" src="assets/img/iconalumni.png">
              </a>
            </div>
            <div class="card-body">
              <div class="card-actions text-center">                  
                <button type="button" class="btn btn-default btn-link" rel="tooltip" data-placement="bottom" title="Login">
                  <a href="alumni">

            <div class="card-header-primary card-header-icon">
                    <div class="card-icon">
                <i class="material-icons"></i>Kunjungi        
                 
                </button>
                
              </div>
              <h4 class="card-title">
                <a><b>Penelusuran Alumni SMK PGRI 35 SOLOKANJERUK</b></a>
              </h4>
              <div class="card-description">
                Silahkan Kunjungi untuk mengetahuinya lebih lanjut
              </div>
            </div>
         
    
      </section>
        <footer class="footer">
  <div class="container-fluid">
    
    <div class="copyright float-right">
      <font color="white"><h4><b>
      &copy;
      <script>
        document.write(new Date().getFullYear())
      </script>, SMK PGRI 35 SOLOKANJERUK by
      <a href="https://www.instagram.com/rijalsetiawann_/" target="_blank" class="text-info">Rizal Setiawan</a></b></h4></font>
    </div>
    <!-- your footer here -->
  </div>
</footer>
</body>

</html>
