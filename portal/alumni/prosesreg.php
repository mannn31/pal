<?php
include './lib/db.class.php';
	$db = new DB();
$informasi = "";

		// Ambil semua variabel kiriman
		// Dasar
		$nama_lengkap   = addslashes($_POST["nama_lengkap"]);
		$nama_panggilan = addslashes($_POST["nama_panggilan"]);
		$jenis_kelamin  = $_POST["jenis_kelamin"];
		$tempat_lahir   = $_POST["tempat_lahir"];
		$tanggal_lahir  = $_POST["tanggal_lahir"];
		$agama          = $_POST["agama"];
		// $foto           = $_POST["foto"];
		$aktif          = $_POST["aktif"];
		$id_provinsi    = $_POST["id_provinsi"];
		$id_kota        = $_POST["id_kota"];
		$alamat         = $_POST["alamat"];
		// Keluarga
		$nama_pasangan  = addslashes($_POST["nama_pasangan"]);
		$nama_anak      = implode("|", $_POST["nama_anak"]);
		// Orang Tua
		$nama_ayah      = addslashes($_POST["nama_ayah"]);
		$nama_ibu       = addslashes($_POST["nama_ibu"]);
		$nama_wali      = addslashes($_POST["nama_wali"]);
		$id_provinsi_ot = $_POST["id_provinsi_ot"];
		$id_kota_ot     = $_POST["id_kota_ot"];
		$alamat_ot      = $_POST["alamat_ot"];
		// Kontak
		$no_rumah       = $_POST["no_rumah_ext"].$_POST["no_rumah"];
		$no_handphone   = $_POST["no_handphone"];
		$no_handphone2  = $_POST["no_handphone2"];
		$pin_blackberry = $_POST["pin_blackberry"];
		$alamat_email   = $_POST["alamat_email"];
		$alamat_website = $_POST["alamat_website"];
		$facebook       = $_POST["facebook"];
		$twitter        = $_POST["twitter"];
		// Akademik
		$angkatan       = $_POST["angkatan"];
		$tahun_masuk    = $_POST["tahun_masuk"];
		$tahun_keluar   = $_POST["tahun_keluar"];
		$kelas_terakhir = $_POST["kelas_terakhir"];
		$catatan        = $_POST["catatan"];
		// Pekerjaan
		$pekerjaan       = $_POST["pekerjaan"];
		$penghasilan    = $_POST["penghasilan"];
		$lama_bekerja   = $_POST["lama_bekerja"];
		$catatan        = $_POST["catatan"];
		// Akun
		$username       = addslashes(strtolower($_POST["username"]));
		$salt           = hash("SHA256", rand());
		$password       = hash("SHA512", $_POST["password"].$salt);
		$level          = "user";

		// Proses upload
		if ($_FILES["foto"]["name"]!="") {
			$lokasi        = "./uploaded/foto_profil/";
			// Check if file is the real image
			$check_image = getimagesize($_FILES["foto"]["tmp_name"]);
			if($check_image !== false) {
				// Verify extension
				$extensions = array("png", "jpg", "jpeg", "gif");
				$file_ext   = explode('.',$_FILES["foto"]["name"]);
				$file_ext   = strtolower(end($file_ext));
				if(in_array($file_ext,$extensions ) === false){
					$errors[] = "<br>Format file tidak diizinkan, format yang diizinkan adalah png, jpg or gif file.";
				}

				// Verify size
				if($_FILES["foto"]["size"] > 2097152){
					$errors[]="<br>Ukuran file melebihi batas 2 MB.";
				}

				// Set new name
				$nama_foto_baru = $username.".".$file_ext;

				// Upload file process
				if(empty($errors)==true){
					// Upload
					move_uploaded_file($_FILES["foto"]["tmp_name"], $lokasi.$nama_foto_baru);
					$sukses_upload = true;
					$foto          = $nama_foto_baru;
				}
				else {
					// Set error count flag and notification
					foreach ($errors as $upload_error) {
						$informasi .= $upload_error;
					}
					$sukses_upload = false;
					$foto          = "no-img.jpg";
				}
			}
		}
		else {
			$sukses_upload = true;
			$foto          = "no-img.jpg";
		}

		// Jika upload berhasil, Proses simpan ~
		if ($sukses_upload) {
			// Simpan Dasar
			$id_anggota = $db->lastInsertId();
			$query      = "INSERT INTO aluni_anggota_dasar VALUES ('$id_anggota', '$nama_lengkap', '$nama_panggilan', '$jenis_kelamin', '$tempat_lahir', '$tanggal_lahir', '$agama', '$foto', '$id_provinsi', '$id_kota', '$alamat', '$aktif', 'NULL', NOW(), 'NULL', NOW(), '0')";
			$proses     = $db->query($query);
			$id_anggota = $db->lastInsertId();

			// Jika anggota berhasil dimasukkan, ambil id_anggota dan simpan data lainnya.
			if ($id_anggota!="") {
				// Simpan Pengguna
				$query  = "INSERT INTO aluni_pengguna VALUES ('$username', '$password', '$salt', '$level', 'ya', '$nama_lengkap', '', '$foto', '$id_anggota', 'NULL', NOW(), 'NULL', NOW(), '0')";
				$proses = $db->query($query);

				// Simpan Hak akses pengguna
				$query  = "INSERT INTO aluni_pengguna_hak_akses VALUES ('$username', '', NOW())";
				$proses = $db->query($query);

				// Simpan status password pengguna
				$query  = "INSERT INTO aluni_pengguna_status_password VALUES ('$id_anggota', '$username', '$_POST[password]', 'belum diubah', 'NULL', NOW(), 'NULL', NOW())";
				$proses = $db->query($query);
				
				// Simpan keluarga
				$query = "INSERT INTO aluni_anggota_keluarga VALUES ('$id_anggota', '$nama_pasangan', '$nama_anak', 'NULL', NOW(), 'NULL', NOW(), '0')";
				$proses     = $db->query($query);
				
				// Simpan orang tua
				$query = "INSERT INTO aluni_anggota_orang_tua VALUES ('$id_anggota', '$nama_ayah', '$nama_ibu', '$nama_wali', '$id_provinsi_ot', '$id_kota_ot', '$alamat_ot', 'NULL', NOW(), 'NULL', NOW(), '0')";
				$proses     = $db->query($query);
				
				// Simpan kontak
				$query = "INSERT INTO aluni_anggota_kontak VALUES ('$id_anggota', '$no_rumah', '$no_handphone', '$no_handphone2', '$pin_blackberry', '$alamat_email', '$alamat_website', '$facebook', '$twitter', 'NULL', NOW(), 'NULL', NOW(), '0')";
				$proses     = $db->query($query);
				
				// Simpan akademik
				$query = "INSERT INTO aluni_anggota_akademik VALUES ('$id_anggota', '$angkatan', '$tahun_masuk', '$tahun_keluar', '$kelas_terakhir', '$catatan', 'NULL', NOW(), 'NULL', NOW(), '0')";
				$proses     = $db->query($query);

				// Simpan pekerjaan
				$query = "INSERT INTO aluni_anggota_pekerjaan VALUES ('$id_anggota', '$pekerjaan', '$penghasilan', '$lama_bekerja', '$catatan', 'NULL', NOW(), 'NULL', NOW(), '0')";
				$proses     = $db->query($query);
			}

			if ($proses) {
				$_SESSION["informasi"] = "Data anggota baru berhasil disimpan!";
			}
			
		}
		// Jika upload gagal, berikan informasi ke halaman depan
		else {
			$_SESSION["informasi"] = "Penyimpanan data anggota gagal!".$informasi;
		}
		header("Location: ./tambah_anggota.php");
		die();
	
?>